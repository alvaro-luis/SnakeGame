using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Serpiente : MonoBehaviour
{
    public GameObject bloque;
    public GameObject Item;
    public GameObject escenario;
    public Text puntuacion;
    public int ancho, alto;
    public float fuerzaExplosion = 200f;

    private Vector3 direccion = Vector3.right;

    private int puntos;

    private enum TipoCasilla
    {
        Vacio, Obstaculo, Item
    }

    private TipoCasilla[,] mapa;

    private Queue<GameObject> cuerpo = new Queue<GameObject>();
    private GameObject cabeza;
    private GameObject item;

    Camera camara;
    [SerializeField] Canvas canvas;

    private void Awake()
    {
        camara = Camera.main;

        mapa = new TipoCasilla[ancho, alto];    
        CrearMuros();
        CentrarCamara();

        int posicionInicialX = ancho / 2;
        int posicionInicialY = alto / 2;

        GameObject bloqueGenerado = null;

        for (int c = 15; c > 0; c--)
        {
            bloqueGenerado = NuevoBloque(posicionInicialX - c, posicionInicialY);
        }

        //cabeza = NuevoBloque(posicionInicialX, posicionInicialY);
        cabeza = bloqueGenerado;
        InstanciarItemEnPosicionAleatoria();
        StartCoroutine(Movimiento());
    }

    private void IncrementarPuntos()
    {
        puntos++;
        puntuacion.text = puntos.ToString();
    }

    private void MoverItemAposicionAleatoria()
    {
        int[] coordenadasVaciasEnEntero = ConvertirEnEnteroPosicionVacia();
        item.transform.position = new Vector2(coordenadasVaciasEnEntero[0], coordenadasVaciasEnEntero[1]);
        EstablecerMapa(item.transform.position, TipoCasilla.Item);
    }

    private void InstanciarItemEnPosicionAleatoria()
    {
        //Posicion vacia
        int[] coordenadasVaciasEnEntero = ConvertirEnEnteroPosicionVacia();

        item = NuevoItem(coordenadasVaciasEnEntero[0], coordenadasVaciasEnEntero[1]);
    }

    private int[] ConvertirEnEnteroPosicionVacia()
    {
        Vector2 posicion = ObtenerPosicionVacia();
        int posXInteger = (int)posicion.x;
        int posYInteger = (int)posicion.y;

        int [] coordenadas = new int[] { posXInteger, posYInteger};

        return coordenadas;
    }

    private Vector2 ObtenerPosicionVacia()
    {
        List<Vector2> posicionesVacias = new List<Vector2>();
        for (int x = 0; x < ancho; x++)
        {
            for (int y = 0; y < alto; y++)
            {
                if (mapa[x,y] == TipoCasilla.Vacio)
                {
                    posicionesVacias.Add(new Vector2(x,y));
                }
            }
        }
        
        return posicionesVacias[UnityEngine.Random.Range(0 , posicionesVacias.Count)];
    }

    private TipoCasilla ObtenerMapa(Vector3 posicion)
    {
        return mapa[Mathf.RoundToInt(posicion.x), Mathf.RoundToInt(posicion.y)];
    }

    private void EstablecerMapa(Vector3 posicion, TipoCasilla valor)
    {
        mapa[Mathf.RoundToInt(posicion.x), Mathf.RoundToInt(posicion.y)] = valor;
    }

    private IEnumerator Movimiento()
    {
        WaitForSeconds espera = new WaitForSeconds(0.15f);
        while (true)
        {
            Vector3 nuevaPosicion = cabeza.transform.position + direccion;
            TipoCasilla casillaAOcupar = ObtenerMapa(nuevaPosicion);
            if (casillaAOcupar == TipoCasilla.Obstaculo)
            {
                Morir();
                yield return new WaitForSeconds(7);
                ReiniciarJuego();
                yield break;
            }
            else
            {   GameObject parteCuerpo;
                if(casillaAOcupar == TipoCasilla.Item)
                {
                    parteCuerpo = NuevoBloque(nuevaPosicion.x, nuevaPosicion.y);
                    MoverItemAposicionAleatoria();
                    IncrementarPuntos();
                }
                else
                {
                    parteCuerpo = cuerpo.Dequeue();
                    EstablecerMapa(parteCuerpo.transform.position, TipoCasilla.Vacio);
                    parteCuerpo.transform.position = nuevaPosicion;
                    EstablecerMapa(nuevaPosicion, TipoCasilla.Obstaculo);
                    cuerpo.Enqueue(parteCuerpo);
                }
                
                cabeza = parteCuerpo;
                yield return espera;
            }
        }
    }

    private void Morir()
    {
        //Vamos a pasar un array de RigidBodys
        Explotar(this.GetComponentsInChildren<Rigidbody>());
        Explotar(escenario.GetComponentsInChildren<Rigidbody>());

        camara.backgroundColor = Color.red;

        float currentRed = puntuacion.color.r;
        float currentGreen = puntuacion.color.g;
        float currentBlue = puntuacion.color.b;

        float newAlpha = 170f / 255f;

        puntuacion.color = new Color(currentRed, currentGreen, currentBlue, newAlpha);
        
    }
    private void Explotar(Rigidbody[] rigidbodys)
    {
        foreach (var rgbd in rigidbodys)
        {
            rgbd.useGravity = true;
            rgbd.AddForce(UnityEngine.Random.insideUnitCircle.normalized * fuerzaExplosion);
        }
    }

    private GameObject NuevoBloque(float x, float y)
    {
        GameObject nuevo = Instantiate(bloque, new Vector3(x, y), Quaternion.identity, this.transform);
        cuerpo.Enqueue(nuevo);
        EstablecerMapa(nuevo.transform.position, TipoCasilla.Obstaculo);
        return nuevo;
    }

    private GameObject NuevoItem(int x, int y)
    {
        GameObject nuevo = Instantiate(Item, new Vector2(x, y), Quaternion.identity, escenario.transform);
        EstablecerMapa(nuevo.transform.position, TipoCasilla.Item);
        return nuevo;
    }

    private void CrearMuros()
    {
        for (int i = 0; i < ancho; i++)
        {
            for (int j = 0; j < alto; j++)
            {
                if (i == 0 || i == ancho - 1 || j == 0 || j == alto - 1)
                {
                    Vector3 posicion = new Vector3(i, j);
                    Instantiate(bloque, posicion, Quaternion.identity, escenario.transform);
                    EstablecerMapa(posicion, TipoCasilla.Obstaculo);
                }
            }
        }
    }

    private void Update()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");
        Vector3 direccionSeleccionada = new Vector3(horizontal, vertical);

        if (direccionSeleccionada != Vector3.zero)
        {
            direccion = direccionSeleccionada;
        }
    }

    private void CentrarCamara()
    {
        //0.5f es el margen que ayuda a encuadrar el centrar de la cámara
        float mitadAncho = (ancho / 2f) - 0.5f;
        float mitadAlto = (alto / 2f) - 0.5f;

        //asignamos los valores obtenidos a las coordenadas de la cámara
        Vector3 posicionCentrada = new Vector3(mitadAncho, mitadAlto, camara.transform.position.z);
        camara.transform.position = posicionCentrada;
        CentrarCanvas(mitadAncho, mitadAlto);
    }

    private void CentrarCanvas(float posX, float posY)
    {
        //Ayuda a centrar texto del puntaje en la pantalla
        puntuacion.alignByGeometry = true;
        
        RectTransform canvasRect = canvas.GetComponent<RectTransform>();
        canvasRect.anchoredPosition = new Vector2(posX,posY);
    }

    
    private void ReiniciarJuego()
    {
        //Se vuelve a cargar la misma escena
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}